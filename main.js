window.onload = () => {
  const batteryElement = document.getElementsByClassName('battery')[0];
  const batteryLevel = Math.floor(Math.random() * 100);
  const batteryWidth = batteryElement.clientWidth;

  const top = batteryWidth - batteryWidth * (batteryLevel / 100);
  batteryElement.style.top = top + 'px';
  batteryElement.style.backgroundColor = 'rgba(200,200,0,0.2)';

  const batteryLevelText = document.createTextNode(batteryLevel + '&#x128267;');
  batteryElement.appendChild(batteryLevelText);

  const batteryStatus = document.createTextNode(
    batteryLevel + '%, not charging'
  );
  document.getElementById('battery-status').appendChild(batteryStatus);

  const hours = new Date().getUTCHours();
  const minutes = new Date().getUTCMinutes();
  const timeStatus = document.createTextNode(hours + ':' + minutes + ' UTC');
  document.getElementById('time').appendChild(timeStatus);
};
